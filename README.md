# Bilgewater
 Unity VR videogame developed by Universidad Politécnica de San Luis Potosi students.
 Bilgewater is a first person game in which you're a sailor trying to escape a haunted abandoned dock while searching for treasures.
 ## Developers ✒️
 * **Oscar Morado Torres** - *180280@upslp.edu.mx* - [OscarMorado](https://github.com/OscarMorado)
 * **Víctor Hugo Jiménez** - *180230@upslp.edu.mx* - [VhugoJc](https://github.com/VhugoJC)
 * **Luis Angel Rodríguez** - *180563@upslp.edu.mx* - [Scrub127](https://github.com/Scrub127)
 * **Manuel Alejandro Martínez** - *180873@upslp.edu.mx* - [alex18M](https://github.com/alex18M)
 * **Jorge Enrique Bravo** - *180980@upslp.edu.mx* - [Rotunno17](https://github.com/Rotunno17)
 * **Sebastian Mata Hernandez** - *170258@upslp.edu.mx* -[Sebastián Mata](https://github.com/SebastianMat4)
