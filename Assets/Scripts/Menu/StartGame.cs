using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartGame : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnCollisionEnter(Collision collision){
        //Add a fadeout animation?
        if(collision.gameObject.name == "Boat"){ //If the player touches the boat, game starts
            SceneManager.LoadScene("Level1");
        }

    }
}
